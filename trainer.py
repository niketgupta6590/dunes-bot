from chatbot import bot
from chatterbot.trainers import ChatterBotCorpusTrainer
from chatterbot.trainers import ListTrainer
import numpy as np
import pandas as pd

xls = pd.read_excel("faq_data.xls")
xls = xls.drop(['Unnamed: 0'], axis=1)
x = xls.values
x = [[i.lower() for i in a] for a in x]

trainer = ListTrainer(bot)
trainer.train(['what is your name?', 'My name is Ideadunes-BOT'])
trainer.train(['who are you?', 'I am a BOT'])
trainer.train(['Bye?', 'Bye, see you later' ])
conversation = [
    "Hello",
    "Hello!!",
    "How are you doing?",
    "I'm doing great.",
    "That is good to hear",
    "Thank you.",
    "You're welcome."
]
trainer.train(conversation)
t = [trainer.train(x[i]) for i in range(0, len(x))]

trainer_corpus = ChatterBotCorpusTrainer(bot)
trainer_corpus.train("chatterbot.corpus.english.greetings",
                     "chatterbot.corpus.english.money",
                     "chatterbot.corpus.hindi")

# "chatterbot.corpus.knowledge.faq"

def brain(user_input):
    response = bot.get_response(user_input)
    return response