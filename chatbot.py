from chatterbot import ChatBot

bot = ChatBot(name = "Ideadunes-BOT", read_only=True,
                 logic_adapters=['chatterbot.logic.MathematicalEvaluation',
                                 {'import_path': 'chatterbot.logic.BestMatch',
                                  'default_response': 'I am sorry, but I do not understand. I am still learning.',
                                  'maximum_similarity_threshold': 0.50},
                                 'chatterbot.logic.TimeLogicAdapter'])